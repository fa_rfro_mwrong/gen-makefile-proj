P = gen-makefile-c
OBJECTS = gen-makefile-proj.o
CFLAGS = -g -Wall -std=c11 -O3
LDFLAGS = 
CC = gcc

all: $(P)

$(P): $(OBJECTS)
	$(CC) $(OBJECTS) $(LDFLAGS) -o $(P)
	rm *.o

$(OBJECTS):
	$(CC) $*.c $(CFLAGS) -c -o $@ 
